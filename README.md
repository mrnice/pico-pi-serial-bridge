# pico-pi-serial-bridge

This is a simple serial bridge to connect e.g a Raspberry PI with the help of a Raspberry Pico to your computer for debugging purposes.

It's a bit of a shame to degrade this fine micro controller to be just a serial connector, but if you, like me, having more micro controllers than serial connectors this project comes handy.

The baudrate is fixed to 115200 and it uses the UART1 pins:

* TX -> GP8
* RX -> GP9

## Getting started

Requirements:
* pico-sdk
* cmake

```
mkdir build
cd build
cmake -GNinja ../
ninja
```

Connect you Pico while pressing the boot pin. Copy the serial-bridge.uf2 to the drive of the pico. this will flash the Pico.

The pico will blink 4 times at startup to show that the serial port ir ready.
## Authors and acknowledgment
This project is based on the [Pico-Three-Way-Serial](https://github.com/HeadBoffin/Pico-Three-Way-Serial) code of Nick McCloud, HeadBoffinWhich is licensed under public domain.
## License
This project is licensed under public domain. See LICENSE file for more information.
