/**
 * Pico Serial bridge, for people like me who are in need of a serial connector.
 *
 * Bernhard Guillon, mrnice, public domain 2021
 *
 * Based on Thre-Way-Serial-Bridge
 * By Nick McCloud, HeadBoffin, 3rd Feb 2021, Unlicensed
 */

#include <stdio.h>
#include "pico/stdlib.h"
#include "pico/stdio.h"
#include "hardware/gpio.h"
#include "hardware/uart.h"

constexpr int baud_rate = 115200;
constexpr int tx_gp = 8;
constexpr int rx_gp = 9;
constexpr int led_gp = 25;

void delay(uint32_t ms) {
    busy_wait_us(ms * 1000);
}

void flash(uint8_t times, uint8_t on = 25, uint8_t off = 100) {
    for (uint8_t c = 0; c < times; c++) {
        gpio_put(led_gp, 1);
        delay(on);
        gpio_put(led_gp, 0);
        delay(off);
    }
}

inline void delay_for_usb_setup()
{
    delay(1000);
}

inline void blink_to_show_we_are_alive()
{
    flash(1);
}

inline void blink_to_show_we_are_set_up()
{
    flash(3);
}

inline void usb_to_uart1()
{
    int16_t ch = getchar_timeout_us(100);
    while (ch != PICO_ERROR_TIMEOUT) {
        uart_putc_raw(uart1, ch);
        ch = getchar_timeout_us(100);
    }
}

inline void uart1_to_usb()
{
    while (uart_is_readable_within_us(uart1, 100)) {
        uint8_t ch = uart_getc(uart1);
        printf("%c", ch);
    }
}

int main() {
    gpio_init(led_gp);
    gpio_set_dir(led_gp, GPIO_OUT);
    blink_to_show_we_are_alive();
    stdio_usb_init();

    uart_init(uart1, baud_rate);
    gpio_set_function(tx_gp, GPIO_FUNC_UART);
    gpio_set_function(rx_gp, GPIO_FUNC_UART);

    delay_for_usb_setup();
    blink_to_show_we_are_set_up();

    while (true) {
        usb_to_uart1();
        uart1_to_usb();
    }

    return 0;
}
